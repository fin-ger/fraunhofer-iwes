profile ?= udp

build:
	@poetry build
	@docker container inspect fraunhofer-iwes_fake-sensor-gpsfake_1 > /dev/null 2>&1 && \
		docker container rm -f fraunhofer-iwes_fake-sensor-gpsfake_1 || true
	@docker-compose --profile $(profile) build

daemon: build
	@docker-compose --profile $(profile) up -d

run: build
	@docker-compose --profile $(profile) up

print-db:
	@docker-compose --profile print-db up

clean:
	@rm -rf ./dist
	@docker network inspect fraunhofer-iwes_seismic-sensor-data > /dev/null 2>&1 && \
		docker network rm fraunhofer-iwes_seismic-sensor-data || true
	@docker-compose down

.PHONY: build daemon run clean
