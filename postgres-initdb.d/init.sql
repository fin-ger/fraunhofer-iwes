create type phoct_status as enum ('T', 'E', 'I');

create table sensor_data (
  id serial primary key,
  time time with time zone,
  utc_status phoct_status,
  latency integer,
  true_heading decimal(6, 3),
  heading_status phoct_status,
  roll_angle decimal(6, 3),
  roll_status phoct_status,
  pitch_angle decimal(5, 3),
  pitch_status phoct_status,
  primary_heave decimal(5, 3),
  heave_status phoct_status,
  heave decimal(5, 3),
  surge decimal(5, 3),
  sway decimal(5, 3),
  heave_speed decimal(5, 3),
  surge_speed decimal(5, 3),
  sway_speed decimal(5, 3),
  heading_rate decimal(6, 2)
)
