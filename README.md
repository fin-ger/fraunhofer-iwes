# Fraunhofer IWES Sensor Data Fetcher

This repository contains

 - a python program that collect sensor data from UDP and GPSD sources
 - a postgresql database configuration
 - docker images for the database, python program, and fake sensor data providers for both UDP and GPSD
 - a docker-compose deployment

## Prerequisites

Before you can start using this application you have to first make sure you have the following applications installed on your system:

 - a docker compatible container runtime (e.g. `docker`, `podman`, etc.)
 - `docker-compose` (tested with v1.28.6)
 - `make`
 - `python-poetry` (tested with v1.1.11)

## Build the application and all the docker images

This project uses `make` to build all the artifacts.

The `docker-compose` deployment comes with 2 different profiles: `udp` and `gpsd`. In order to build the `udp` deployment, run

```
$ make build profile=udp
```

And to build the `gpsd` deployment, run

```
$ make build profile=gpsd
```

When not providing a profile `udp` is used by default.

## Run the deployment

To run the `udp` profile, run

```
$ make run profile=udp
```

And to run the `gpsd` profile, run

```
$ make run profile=gpsd
```

## Quickly show the database contents

You can run `make print-db` to show the contents of the database. It is better to run this when the deployment is currently not running as the command will block until the database shuts down.

## Persisting the database

Uncomment `line 14` in the `docker-compose.yml`:

```
14      #- ./db-data:/var/lib/postgresql/data:Z
```

This will mount the postgresql data directory into your local workdir.

## Notes

### Note on the GPSD integration

Currently `GPSD` in not capable of parsing the sensor format. Therefore, this application reads the sensor information as raw messages from `GPSD` which are naturally not normalized by `GPSD`. Using the `GPSD` currently has no real benefit over a raw `UDP` socket connection to the sensor.

### Performance note

Currently, the application parses the incoming data from the sensor and places the parsed message into the database. This makes aggregation over the data quite easy but comes with several drawbacks:

1. the parsing adds additional calculations before inserting a received sensor message into the database and is therefore slower
2. parsing can fail if the sensor message is corrupted or not in the expected format leading to loss of the sensor message (not inserted into the database, but full message logged on failure)

Depending on the requirements, the parsing of the message may therefore be removed.
