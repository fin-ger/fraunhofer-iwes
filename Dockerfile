from python:3.9-alpine

copy dist/fraunhofer-iwes-0.1.0.tar.gz /tmp
run pip install /tmp/fraunhofer-iwes-0.1.0.tar.gz

cmd fraunhofer-iwes-collector
