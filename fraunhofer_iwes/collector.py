"""
This program grabs seismic sensor imformation from a UDP stream or a GPSD and writes them to a
Postgresql database.
"""

import signal
import asyncio
import traceback

import configargparse
import psycopg

from .logging import LOG_LEVELS, ExitCode, init_log
from .gpsdclient import GPSDClient
from .udpclient import UDPClient
from .dbclient import DBClient

def cli():
    """
    Main entrance point for the cli.
    """
    asyncio.run(main())

async def main():
    """
    Async main cannot be directly used as a script entrance point. Use `.collector:cli()` instead.
    """

    # create command line interface
    parser = configargparse.ArgumentParser(
        add_env_var_help=True,
        description="Fraunhofer IWES seismic sensor data collector"
    )
    parser.add_argument(
        "-l",
        "--log-level",
        default="INFO",
        env_var="LOG_LEVEL",
        choices=LOG_LEVELS,
        metavar="LEVEL",
        help="the log level on stderr for this application",
    )
    parser.add_argument(
        "--postgres-host",
        required=True,
        env_var="POSTGRES_HOST",
        metavar="HOSTNAME",
        help="the hostname of the postgres server",
    )
    parser.add_argument(
        "--postgres-db",
        required=True,
        env_var="POSTGRES_DB",
        metavar="NAME",
        help="the database name of the postgres server",
    )
    parser.add_argument(
        "--postgres-user",
        required=True,
        env_var="POSTGRES_USER",
        metavar="USERNAME",
        help="the username used to connect to the postgres database",
    )
    parser.add_argument(
        "--postgres-password",
        required=True,
        env_var="POSTGRES_PASSWORD",
        metavar="PASSWORD",
        help="the password used to connect to the postgres database",
    )
    parser.add_argument(
        "--gpsd-host",
        env_var="GPSD_HOST",
        metavar="HOSTNAME",
        help="the hostname of the gpsd server",
    )
    parser.add_argument(
        "--gpsd-port",
        env_var="GPSD_PORT",
        metavar="PORT",
        default="2947",
        help="the tcp port of the gpsd server",
    )
    parser.add_argument(
        "--udp-host",
        env_var="UDP_HOST",
        metavar="HOSTNAME",
        help="the hostname of the udp server",
    )
    parser.add_argument(
        "--udp-port",
        env_var="UDP_PORT",
        metavar="PORT",
        default="1337",
        help="the port of the udp server",
    )
    parser.add_argument(
        "-m",
        "--collect-mode",
        env_var="COLLECT_MODE",
        metavar="MODE",
        choices=("udp", "gpsd"),
        default="udp",
        help="the mode used for collecting sensor data",
    )

    options = parser.parse_args()

    # defines builtins.log globally
    init_log(options)

    if options.log_level not in LOG_LEVELS:
        log.warning("Invalid log level %s! Valid choices are %s", options.log_level, {LOG_LEVELS})
        log.warning("Using default log level instead...")
        options.log_level = "INFO"

    # all sensor data is buffered between the receiving part from the udp socket
    # or gpsd tcp stream and the transmitting part of the database
    message_queue = asyncio.Queue()

    # start appropriate workers for each collection mode
    if options.collect_mode == "udp":
        if "udp_host" not in options:
            parser.error("udp-host not provided while using udp collect-mode!")

        collect_task = asyncio.create_task(
            udp_worker(message_queue, host=options.udp_host, port=options.udp_port)
        )

    elif options.collect_mode == "gpsd":
        if "gpsd_host" not in options:
            parser.error("gpsd-host not provided while using gpsd collect-mode!")

        collect_task = asyncio.create_task(
            gpsd_worker(message_queue, host=options.gpsd_host, port=options.gpsd_port)
        )
    else:
        parser.error(f"Invalid collect-mode {options.collect_mode}!")

    # start database worker
    db_task = asyncio.create_task(
        db_worker(
            message_queue,
            host=options.postgres_host,
            db=options.postgres_db,
            user=options.postgres_user,
            password=options.postgres_password,
        )
    )

    def signal_handler(_sig, _frame):
        collect_task.cancel()

    # handle signals for graceful shutdown
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    # await workers until completion
    await collect_task
    db_task.cancel()
    await db_task

    log.info("Quitting...")

async def db_worker(message_queue, host, db, user, password):
    """
    This worker writes messages coming from the message_queue to the postgres database.
    """

    connection_retries = 0

    # attempt to connect 10 times until the program exits with an error code
    while True:
        try:
            log.info("Connecting to database...")
            database = await DBClient.new(host, db, user, password)
            connection_retries = 0

            log.info("Start writing messages to database...")
            while True:
                message = await message_queue.get()
                await database.write_message(message)
                message_queue.task_done()

            log.info("Closing database connection")
            await database.close()

        except psycopg.OperationalError:
            if connection_retries >= 10:
                log.bail(
                    f"Postgresql database not responding on {host} after 10 retries",
                    ExitCode.DATABASE_NOT_AVAILABLE,
                )

            log.warning("Postgresql not running, reconnecting in 3s...")
            connection_retries += 1
            await asyncio.sleep(3)
            continue
        except Exception as ex:
            trace = traceback.format_exc()
            log.bail(
                f"Database worker thread encountered an error: {ex}\n{trace}",
                ExitCode.UNEXPECTED_ERROR,
            )

async def udp_worker(message_queue, host, port):
    """
    This worker reads messages from the given UDP socket and writes them to the message_queue.
    """

    connection_retries = 0

    # attempt to connect 10 times until the program exits with an error code
    while True:
        log.info("Connecting to udp socket at udp://%s:%s...", host, port)
        try:
            udp = await UDPClient.new(host=host, port=port)
            connection_retries = 0
            log.info("Established connection to udp host!")

            async for message in udp.stream():
                message_queue.put_nowait(message)

            log.info("No more messages from udp stream, quitting...")
            udp.close()
            break
        except ConnectionRefusedError:
            if connection_retries >= 10:
                log.bail(
                    f"UDP host not responding on {host}:{port} after 10 retries",
                    ExitCode.GPSD_NOT_AVAILABLE,
                )

            log.warning("UDP host not running, reconnecting in 3s...")
            connection_retries += 1
            await asyncio.sleep(3)
            continue
        except Exception as ex:
            trace = traceback.format_exc()
            log.bail(
                f"UDP worker thread encountered an error: {ex}\n{trace}",
                ExitCode.UNEXPECTED_ERROR,
            )

async def gpsd_worker(message_queue, host, port):
    """
    This worker reads messages from gpsd and writes them to the message_queue.
    """
    connection_retries = 0

    # attempt to connect 10 times until the program exits with an error code
    while True:
        log.info("Connecting to gpsd as tcp://%s:%s...", host, port)
        try:
            gpsd = await GPSDClient.new(host=host, port=port)
            connection_retries = 0
            log.info("Established connection to gpsd!")

            async for message in gpsd.stream():
                message_queue.put_nowait(message)

            log.info("No more messages from gpsd, quitting...")
            await gpsd.close()
            break
        except ConnectionRefusedError:
            if connection_retries >= 10:
                log.bail(
                    f"GPSD host not responding on {host}:{port} after 10 retries",
                    ExitCode.GPSD_NOT_AVAILABLE,
                )

            log.warning("GPSD host not running, reconnecting in 3s...")
            connection_retries += 1
            await asyncio.sleep(3)
            continue
        except Exception as ex:
            trace = traceback.format_exc()
            log.bail(
                f"GPSD worker thread encountered an error: {ex}\n{trace}",
                ExitCode.UNEXPECTED_ERROR,
            )
