"""
Types related to the database connection.
"""

from psycopg import AsyncConnection

from .phoct import PhoctStatus, PhoctStatusDumper

class DBClient:
    """
    Connect to the Postgresql database and write sensor messages the sensor_data table.
    """

    @classmethod
    async def new(cls, host, db, user, password):
        """
        Create a new DBClient connected to the given database.
        """

        self = DBClient()
        conn_string = f"host={host} dbname={db} user={user} password={password} connect_timeout=10"

        self.conn = await AsyncConnection.connect(conn_string)
        log.info("Connected to postgresql!")

        self.conn.adapters.register_dumper(PhoctStatus, PhoctStatusDumper)

        return self

    def __init__(self):
        self.conn = None

    async def write_message(self, message):
        """
        Write a message to the sensor_data table of the database.
        """

        async with self.conn.cursor() as cur:
            await cur.execute(
                "insert into sensor_data (" \
                "  time, utc_status, latency, true_heading, heading_status, " \
                "  roll_angle, roll_status, pitch_angle, pitch_status, " \
                "  primary_heave, heave_status, heave, surge, sway, " \
                "  heave_speed, surge_speed, sway_speed, heading_rate" \
                ") values (" \
                "  %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s" \
                ")",
                (
                    message.time,
                    message.utc_status,
                    message.latency,
                    message.true_heading,
                    message.heading_status,
                    message.roll_angle,
                    message.roll_status,
                    message.pitch_angle,
                    message.pitch_status,
                    message.primary_heave,
                    message.heave_status,
                    message.heave,
                    message.surge,
                    message.sway,
                    message.heave_speed,
                    message.surge_speed,
                    message.sway_speed,
                    message.heading_rate,
                ),
            )

            await self.conn.commit()

    async def close(self):
        """
        Close the database connection.
        """

        await self.conn.close()
