"""
The logging module.
"""

import sys
import logging
import types
import builtins

from enum import Enum

LOG_LEVELS = ("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL")

class ExitCode(Enum):
    """
    An enum of all exit codes for this application.
    """

    SUCCESS = 0
    GPSD_NOT_AVAILABLE = 3
    DATABASE_NOT_AVAILABLE = 4
    UNEXPECTED_ERROR = 5

class ColorFormatter(logging.Formatter):
    """
    A logging formatter for the logger.
    """

    cyan        = "\x1b[0;36m"
    green       = "\x1b[0;32m"
    bold_yellow = "\x1b[1;33m"
    red         = "\x1b[0;31m"
    bold_red    = "\x1b[1;31m"
    purple      = "\x1b[0;35m"
    bold_purple = "\x1b[1;35m"
    reset       = "\x1b[0m"
    width       = "8.8"

    FORMATS = {
        logging.DEBUG   : f"{cyan}%(levelname){width}s{reset} %(message)s",
        logging.INFO    : f"{green}%(levelname){width}s{reset} %(message)s",
        logging.WARNING : f"{bold_yellow}%(levelname){width}s{reset} %(message)s",
        logging.ERROR   : f"{bold_red}%(levelname){width}s{red} %(message)s{reset}",
        logging.CRITICAL: f"{bold_purple}%(levelname){width}s{purple} %(message)s{reset}",
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

def bail(self, msg, code):
    """
    Print a critical log message and exit the application with the given exit code.
    """
    self.critical(f"{msg}, exiting with {code.name}:{code.value}...")
    sys.exit(code.value)

def init_log(options):
    """
    Initialize the logger globally in builtins from the provided options.
    """
    log = logging.getLogger("fraunhofer-iwes-collector")
    log.setLevel(options.log_level)
    hdl = logging.StreamHandler()
    hdl.setLevel(logging.DEBUG)
    hdl.setFormatter(ColorFormatter())
    log.addHandler(hdl)
    log.bail = types.MethodType(bail, log)

    builtins.log = log
