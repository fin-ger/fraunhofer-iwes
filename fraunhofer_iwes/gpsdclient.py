"""
Types related to the GPSD connection.
"""

import json
import asyncio

from .phoct import PhoctMessage, NotAPhoctMessageError, ChecksumError

class GPSDClient:
    """
    Connect to the GPSD server and fetch sensor data from it.
    """

    @classmethod
    async def new(cls, host="127.0.0.1", port="2947"):
        """
        Create a new GPSDClient connected to GPSD.
        """

        self = GPSDClient()
        self.host = host
        self.port = port
        self.reader, self.writer = await asyncio.open_connection(self.host, int(self.port))

        return self

    def __init__(self):
        self.host = None
        self.port = None
        self.reader = None
        self.writer = None

    async def stream(self):
        """
        Fetch messages from GPSD and yield them in a stream.
        """

        version_msg = json.loads((await self.reader.readline()).decode("utf-8"))
        if version_msg["class"] != "VERSION":
            log.warning("Expected version message, received %s instead", version_msg)
        else:
            log.debug("Received version message")

        self.writer.write(b'?WATCH={"enable":true,"raw":1}\n')
        await self.writer.drain()

        devices_msg = json.loads((await self.reader.readline()).decode("utf-8"))
        if devices_msg["class"] == "DEVICES" and len(devices_msg["devices"]) == 0:
            log.warning("No devices connected to gpsd")
        else:
            log.debug("Received devices message")

        watch_msg = json.loads((await self.reader.readline()).decode("utf-8"))
        if watch_msg["class"] != "WATCH" or not watch_msg["enable"] or watch_msg["raw"] != 1:
            log.warning("Unexpected response from gpsd for watch message: %s", watch_msg)
        else:
            log.debug("Received watch message")

        while True:
            raw_message = (await self.reader.readline()).decode("utf-8")

            # if stream closed
            if len(raw_message) == 0:
                break

            try:
                yield PhoctMessage(raw_message)
            except ChecksumError:
                log.warning("Received corrupted message %s", raw_message)
            except NotAPhoctMessageError:
                log.warning("Could not parse message %s", raw_message)

    async def close(self):
        """
        Close the connection to GPSD.
        """

        if self.writer:
            self.writer.close()
            await self.writer.wait_closed()
        self.writer = None
        self.reader = None

    def __str__(self):
        return "<GPSDClient(host=%s, port=%s)>" % (self.host, self.port)
