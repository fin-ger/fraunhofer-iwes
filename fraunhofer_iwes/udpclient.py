"""
This module contains all types related to the UDP client.
"""

import asyncudp

from .phoct import PhoctMessage, NotAPhoctMessageError

# I don't know how to establish a transport with the sensor,
# so just sending a dummy message here for ncat
WELCOME_MSG = b"hello\n"

class LineBuffer:
    """
    A simple buffer collecting chunks until a whole line was received.
    """
    def __init__(self):
        self._buffer = bytes()

    def write(self, data):
        """
        Write a binary chunk to the buffer
        """
        self._buffer += data

    def readline(self):
        """
        Read a completely received line from the buffer.

        If no complete line is available None is returned.
        """

        idx = self._buffer.find(b"\r\n")
        if idx is not -1:
            line = self._buffer[:idx+2]
            self._buffer = self._buffer[idx+2:]
            return line.decode("utf-8")

        return None

class UDPClient:
    """
    Connect to the provided UDP socket and fetch sensor data from it.
    """

    @classmethod
    async def new(cls, host="127.0.0.1", port="1337"):
        """
        Create a new UDPClient connected to the provided UDP socket.
        """
        self = UDPClient()
        self.host = host
        self.port = port
        self.sock = await asyncudp.create_socket(remote_addr=(self.host, int(self.port)))
        self.buffer = LineBuffer()

        return self

    def __init__(self):
        self.host = None
        self.port = None
        self.sock = None
        self.buffer = None

    async def stream(self):
        """
        Fetch messages from the UDP socket and yield them in a stream.
        """

        self.sock.sendto(WELCOME_MSG)

        while True:
            udp_packet, _addr = await self.sock.recvfrom()
            self.buffer.write(udp_packet)
            raw_message = self.buffer.readline()
            if raw_message is None:
                # line not complete
                continue

            try:
                yield PhoctMessage(raw_message)
            except NotAPhoctMessageError:
                log.warning(f"Could not parse message {raw_message}")

    def close(self):
        """
        Close the UDP socket.
        """
        self.sock.close()

    def __str__(self):
        return "<UDPClient(host=%s, port=%s)>" % (self.host, self.port)
