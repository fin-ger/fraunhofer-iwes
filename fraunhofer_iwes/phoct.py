"""
This module contains all types related to parsing the sensor messages.
"""

import re
import functools

from datetime import time, timezone
from decimal import Decimal
from enum import Enum

from psycopg.adapt import Dumper, Loader

# format taken from here: https://support.sbg-systems.com/sc/dev/files/latest/20940670/20940669/1/1626942673538/Ellipse+Ekinox+Apogee+Series+-+Firmware+Manual.pdf pylint: disable=C0301
PHOCT_RE = re.compile(r"""(?x)
\$PHOCT,                              # message id - lxblue proprietary NMEA identifier
01,                                   # protocol version identifier

(?P<time_h>\d{2})                     # UTC time, current time
(?P<time_m>\d{2})
(?P<time_s>\d{2})\.
(?P<time_us>\d+),

(?P<utc_status>[TEI]),                # UTC time status
(?P<latency>\d{2}),                   # INS latency for heading, roll, pitch (not implemented, always set to 0)
(?P<true_heading>\d{3}\.\d{3}),       # true heading in degree (from 000.000 to 359.999)
(?P<heading_status>[TEI]),            # true heading status
(?P<roll_angle>[+-]?\d{3}\.\d{3}),    # roll in degree (postive if port side up) [-180.000;+180.000]
(?P<roll_status>[TEI]),               # roll status
(?P<pitch_angle>[+-]?\d{2}\.\d{3}),   # pitch degree (positive if bow down) [-90.000;+90.000]
(?P<pitch_status>[TEI]), # Pitch status
(?P<primary_heave>[+-]?\d{2}\.\d{3}), # heave at primary lever arm in meters (postive up) [-99.999;+99.999]
(?P<heave_status>[TEI]),              # heave status (also for surge, sway & speed)
(?P<heave>[+-]?\d{2}\.\d{3}),         # heave at desired lever arm in meters (positive up) [-99.999;+99.999]
(?P<surge>[+-]?\d{2}\.\d{3}),         # surge with lever arms applied in meters (positive forward) [-99.999;+99.999]
(?P<sway>[+-]?\d{2}\.\d{3}),          # sway at desired lever arm in meters (postive left) [-99.999;+99.999]
(?P<heave_speed>[+-]?\d{2}\.\d{3}),   # heave speed at desired lever arm in m/s (positive up) [-99.999;+99.999]
(?P<surge_speed>[+-]?\d{2}\.\d{3}),   # surge speed at desired lever arm in m/s (positive forward) [-99.999;+99.999]
(?P<sway_speed>[+-]?\d{2}\.\d{3}),    # sway speed at desired lever arm in m/s (positive left) [-99.999;+99.999]
(?P<heading_rate>[+-]?\d{4}\.\d{2})   # heading rate of turn in °/min (positive clockwise) [-9999.99;+9999.99]
\*(?P<checksum>[0-9A-F]{2})           # xor of all previous bytes
\r\n$
""")

class PhoctStatus(Enum):
    """
    An enum representing a PHOCT status field.
    """

    VALID = 'T'
    INVALID = 'E'
    INITIALIZING = 'I'

    def __init__(self, status_value):
        self.status = status_value

    def quality_indicator(self):
        """
        Retrieve the quality indicator for this status.

        'T' => 1
        'E' => -1
        'I' => -2
        """

        if self.status == 'T':
            return 1.0
        if self.status == 'E':
            return -1.0
        if self.status == 'I':
            return -2.0

        raise Exception(f"invalid PHOCT status {self.status}!")

    def __str__(self):
        return self.status

class PhoctStatusDumper(Dumper): # pylint: disable=R0903
    """
    This type enables placing a PhoctStatus directly into Postgresql.
    """
    def dump(self, status): # pylint: disable=R0201
        """
        Write the status field out as an utf-8 string.
        """
        return bytes(status.status, encoding="utf8")

class PhoctStatusLoader(Loader): # pylint: disable=R0903
    """
    This type enables loading a PhoctStatus directly from Postgresql.
    """
    def load(self, data): # pylint: disable=R0201
        """
        Create a PhoctStatus from the given data string.
        """
        return PhoctStatus(data)

class NotAPhoctMessageError(Exception):
    """
    This error indicates that parsing of an incoming message failed.
    """

    def __init__(self, message):
        super().__init__(f"The message {message} is not a valid PHOCT message!")

class ChecksumError(Exception):
    """
    This error indicates that the checksum of the received message was not correct.
    """

    def __init__(self, message):
        super().__init__(f"The message {message} has an invalid checksum!")

class PhoctMessage: # pylint: disable=R0903,R0902
    """
    This type represents a complete PHOCT message from a sensor.
    """

    def __init__(self, raw_message):
        match = PHOCT_RE.fullmatch(raw_message)
        if match is None:
            raise NotAPhoctMessageError(raw_message)

        checksum = int(match["checksum"], base=16)
        payload = bytes(match.string[1:match.start("checksum")-1], encoding="utf-8")
        xor = functools.reduce(lambda a, b: a ^ b, payload)
        if checksum != xor:
            raise ChecksumError(raw_message)

        self.time = time(
            int(match["time_h"]),
            int(match["time_m"]),
            int(match["time_s"]),
            int(match["time_us"]),
            timezone.utc,
        )
        self.utc_status = PhoctStatus(match["utc_status"])
        self.latency = int(match["latency"])
        self.true_heading = Decimal(match["true_heading"])
        self.heading_status = PhoctStatus(match["heading_status"])
        self.roll_angle = Decimal(match["roll_angle"])
        self.roll_status = PhoctStatus(match["roll_status"])
        self.pitch_angle = Decimal(match["pitch_angle"])
        self.pitch_status = PhoctStatus(match["pitch_status"])
        self.primary_heave = Decimal(match["primary_heave"])
        self.heave_status = PhoctStatus(match["heave_status"])
        self.heave = Decimal(match["heave"])
        self.surge = Decimal(match["surge"])
        self.sway = Decimal(match["sway"])
        self.heave_speed = Decimal(match["heave_speed"])
        self.surge_speed = Decimal(match["surge_speed"])
        self.sway_speed = Decimal(match["sway_speed"])
        self.heading_rate = Decimal(match["heading_rate"])

    def __str__(self):
        return "<PhoctMessage(" \
            "time=%s, utc_status=%s, latency=%s, true_heading=%s, heading_status=%s, " \
            "roll_angle=%s, roll_status=%s, pitch_angle=%s, pitch_status=%s, " \
            "primary_heave=%s, heave_status=%s, heave=%s, surge=%s, sway=%s, " \
            "heave_speed=%s, surge_speed=%s, sway_speed=%s, heading_rate=%s)>" \
            % (self.time, self.utc_status, self.latency, self.true_heading, self.heading_status,
               self.roll_angle, self.roll_status, self.pitch_angle, self.pitch_status,
               self.primary_heave, self.heave_status, self.heave, self.surge, self.sway,
               self.heave_speed, self.surge_speed, self.sway_speed, self.heading_rate)
